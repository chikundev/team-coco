-- chikun :: 2014
-- Dialogue template


return {
    taunts = {
        argument = {
            'Why would that even work?',
            'It would have been better if you tried another way',
            'Leaving things the way they are is inferior'
        },

        fallacy = {
            "That can't work because it's you",
            'My friend says that this way is superior',
            "That's just a failure of a failure"
        },

        counterFallacy = {
            "The logic doesn't follow",
            "Your argument is irrelevant",
            "That particular person is not of a relevant authority"
        },

        research = {
            'Strength in preparation',
            'Knowledge is power',
            'Research makes my logic more powerful'
        },

    }
}
