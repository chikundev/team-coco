-- chikun :: 2014
-- enemy functions

-- table for enemies and their functions
enemy = { }
enemies = { }
enemies.type = {
    hater = {
        code = "hater",
        name = "Hater",
        image = gfx.hater,
        imageCombat = gfx.troll2
    },
    troll = {
        code = "troll",
        name = "Troll",
        image = gfx.troll,
        imageCombat = gfx.troll2
    }
}


function enemies.load(objects)

    for key, object in ipairs(objects) do

        -- Figure out facing of enemy
        local dir = 0
        if object.name == "right" then
            dir = 90
        elseif object.name == "down" then
            dir = 180
        elseif object.name == "left" then
            dir = 270
        end

        -- All info for current enemy
        local newEnemy = {
            x = object.x,
            y = object.y,
            w = object.w,
            h = object.h,
            type = enemies.type[object.type],
            direction = dir,
        }

        if (dir % 180) > 0 then
            -- xAxis
            local poll = 0
            if dir == 270 then
                poll = - 1
            else
                poll = 1
            end
            local dot = {
                x = object.x + object.w / 2 + poll * object.w / 2,
                y = object.y + object.h / 2,
                w = 0,
                h = 0
            }
            while not math.overlapTable(collisions, dot) do
                dot.x = dot.x + poll * 8
            end
            if poll == 1 then
                newEnemy.wall = {
                    x = object.x + object.w / 2 + poll * object.w / 2,
                    y = object.y + object.h / 2,
                    w = dot.x - (object.x + object.w / 2 + poll * object.w / 2),
                    h = 1
                }
            else
                newEnemy.wall = {
                    x = dot.x,
                    y = object.y + object.h / 2,
                    w = math.abs(dot.x - (object.x + object.w / 2 +
                            poll * object.w / 2)),
                    h = 1
                }
            end
        else
            -- yAxis
            local poll = 0
            if dir == 0 then
                poll = - 1
            else
                poll = 1
            end
            local dot = {
                x = object.x + object.w / 2,
                y = object.y + object.h / 2 + poll * object.h / 2,
                w = 0,
                h = 0
            }
            while not math.overlapTable(collisions, dot) do
                dot.y = dot.y + poll * 8
            end
            if poll == 1 then
                newEnemy.wall = {
                    x = object.x + object.w / 2,
                    y = object.y + object.h / 2 + poll * object.h / 2,
                    w = 1,
                    h = dot.y - (object.y + object.h / 2 + poll * object.h / 2)
                }
            else
                newEnemy.wall = {
                    x = object.x + object.w / 2,
                    y = dot.y,
                    w = 1,
                    h = math.abs(dot.y - (object.y + object.h / 2 +
                                poll * object.h / 2))
                }
            end
        end
        table.insert(enemy, newEnemy)
    end

end


function enemies.update(dt)



end


function enemies.draw()

    for key, object in ipairs(enemy) do
        g.setColor(255, 255, 255)
        g.draw(object.type.image, object.x + object.w / 2, object.y + object.h / 2,
            math.rad(object.direction), 1, 1, object.w / 2, object.h / 2)
    end

end
