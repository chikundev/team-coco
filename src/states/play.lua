-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }


-- Table for Player
player = { }

-- Table for the exit
exit = { }

-- Table for enemies and one for their functions
enemy   = { }

-- On state create
function newState:create()

    -- Table for collidable objects
    collisions = { }

    -- Interpret the map
    for key, value in ipairs(map.current.layers) do

        -- Place in all walls
        if value.name == "collisions" then
            collisions = value.objects

        -- Place in important objects
        elseif value.name == "important" then
            for key, object in ipairs(value.objects) do

                -- Place in Player
                if object.name == "spawn" then
                    player.create(
                        object.x,
                        object.y,
                        object.w,
                        object.h
                    )
                elseif object.name == "exit" then
                    local dir = 0
                    if object.type == "right" then
                        dir = 90
                    elseif object.type == "down" then
                        dir = 180
                    elseif object.type == "left" then
                        dir = 270
                    end
                    exit = {
                        x = object.x,
                        y = object.y,
                        w = object.w,
                        h = object.h,
                        direction = dir,
                        image = gfx.hater
                    }
                    if (dir % 180) > 0 then
                        -- xAxis
                        local poll = 0
                        if dir == 270 then
                            poll = - 1
                        else
                            poll = 1
                        end
                        local dot = {
                            x = object.x + object.w / 2 + poll * object.w / 2,
                            y = object.y + object.h / 2,
                            w = 0,
                            h = 0
                        }
                        while not math.overlapTable(collisions, dot) do
                            dot.x = dot.x + poll * 8
                        end
                        if poll == 1 then
                            wxit.wall = {
                                x = object.x + object.w / 2 + poll * object.w / 2,
                                y = object.y + object.h / 2,
                                w = dot.x - (object.x + object.w / 2 + poll * object.w / 2),
                                h = 1
                            }
                        else
                            exit.wall = {
                                x = dot.x,
                                y = object.y + object.h / 2,
                                w = math.abs(dot.x - (object.x + object.w / 2 +
                                        poll * object.w / 2)),
                                h = 1
                            }
                        end
                    else
                        -- yAxis
                        local poll = 0
                        if dir == 0 then
                            poll = - 1
                        else
                            poll = 1
                        end
                        local dot = {
                            x = object.x + object.w / 2,
                            y = object.y + object.h / 2 + poll * object.h / 2,
                            w = 0,
                            h = 0
                        }
                        while not math.overlapTable(collisions, dot) do
                            dot.y = dot.y + poll * 8
                        end
                        if poll == 1 then
                            exit.wall = {
                                x = object.x + object.w / 2,
                                y = object.y + object.h / 2 + poll * object.h / 2,
                                w = 1,
                                h = dot.y - (object.y + object.h / 2 + poll * object.h / 2)
                            }
                        else
                            exit.wall = {
                                x = object.x + object.w / 2,
                                y = dot.y,
                                w = 1,
                                h = math.abs(dot.y - (object.y + object.h / 2 +
                                            poll * object.h / 2))
                            }
                        end
                    end
                end
            end

        -- Place in all enemies
        elseif value.name == "enemies" then
            enemies.load(value.objects)
        end

    end

    view = {
        x = 0,
        y = 0,
        w = 320,
        h = 240
    }

end


-- On state update
function newState:update(dt)

    player.update(dt)

    view.x = player.x - (view.w - player.w) / 2
    view.y = player.y - (view.h - player.h) / 2
    view.x = math.clamp(0, view.x, map.current.w * map.current.tileW - view.w)
    view.y = math.clamp(0, view.y, map.current.h * map.current.tileH - view.h)

end


-- On state draw
function newState:draw()

    scale.perform()

    g.translate(-view.x, -view.y)

    g.setColor(255, 255, 255)
    map.draw(map.current)

    player.draw()
    enemies.draw()
    g.setColor(255, 255, 255)
    g.draw(exit.image, exit.x + exit.w / 2, exit.y + exit.h / 2,
        math.rad(exit.direction), 1, 1, exit.w / 2, exit.h / 2)

    g.origin()

end


-- On state kill
function newState:kill()

    enemy = { }
    exit = { }
    levels[lvl.current].bgm:stop()

end


-- Transfer data to state loading script
return newState
