-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }
local timer = 0
local timerMax = 1.5
local miniTimer = 0
local miniTimerMax = 0.3
local sight = 255
local enemyDMG = 0
local playerDMG = 0
local rotation = 0

-- On state create
function newState:create()

    rotation = 0
    timer = timerMax
    minitimer = 0
    sight = 255

    -- SHOULD be setting if flashing red or green
    if hp.enemy.previous > hp.enemy.current then
        print('a')
        enemyDMG = - 1
    elseif hp.enemy.previous < hp.enemy.current then
        print('b')
        enemyDMG = 1
    else
        print('c')
        enemyDMG = 0
    end
    if hp.player.previous > hp.player.current then
        print('d')
        playerDMG = - 1
    elseif hp.player.previous < hp.player.current then
        print('e')
        playerDMG = 1
    else
        print('f')
        playerDMG = 0
    end
    print(enemyDMG)
    print(hp.enemy.previous)
    print(hp.enemy.current)
    print(playerDMG)
    print(hp.player.previous)
    print(hp.player.current)

end


-- On state update
function newState:update(dt)

    -- Spinning duriong research
    if lastAttack == 'research' then
        rotation = rotation + (2 * math.pi * dt / timerMax)
    end

    -- counting down through timer
    if timer > 0 then
        timer = timer - dt
        -- starting up flashings
        if timer <= timerMax / 2 and miniTimer == 0 then
            miniTimer = miniTimerMax
            -- No flash if researching
            if lastAttack == 'research' then
                sight = 255
            else
                sight = 0
            end
        end
        -- prevent timer doing wierd shit
        if timer <= 0 then
            timer = 0
        end
    -- change state
    else
        hp.player.previous = hp.player.current
        hp.enemy.previous = hp.enemy.current
        if who == 0 then
            state.current = states.combat
        else
            state.current = states.combatEnemy
        end
    end

    -- prevent flashing till half way through
    if timer > timerMax / 2 then
        miniTimer = 0
    end

    -- count down mini timer
    if miniTimer > 0 then
        miniTimer = miniTimer - dt
        -- reset mini timer
        if miniTimer <= 0 then
            -- prevent flashing if research
            if lastAttack == 'research' then
                sight = 255
            else
                sight = 0
            end
            miniTimer = miniTimerMax
        end
    end

    -- increase sight so it actually fads back in
    if sight < 255 then
        sight = sight + (255 * dt / miniTimerMax)
        if sight >= 255 then
            sight = 255
        end
    end

end


-- On state draw
function newState:draw()

    scale.perform()
    g.setFont(fnt.option)
    g.setColor(255, 255, 255)
    g.rectangle('fill', 0, 0, 320, 240)

    -- Change enemy color
    if timer < timerMax / 2 then
         g.setColor(255 * (1 - math.max(enemyDMG, 0)),
                    255 * (1 + math.min(0, enemyDMG)),
                    255 * (1 - math.abs(enemyDMG)),
                    sight)
    else
        g.setColor(255, 255, 255)
    end

    if who == 0 then
        if lastAttack == 'research' then
            g.draw(enemy[enemyCurrent].type.imageCombat, 260, 50, rotation, 1, 1, 40, 40)
        else
            g.draw(enemy[enemyCurrent].type.imageCombat, 200 + math.abs(((timer * ((2 - timerMax)/timerMax + 1)) * 20) - 20), 10)
        end
    else
        g.draw(enemy[enemyCurrent].type.imageCombat, 220, 10)
    end


    if timer < timerMax / 2 then
         g.setColor(255 * (1 - math.max(playerDMG, 0)),
                    255 * (1 + math.min(0, playerDMG)),
                    255 * (1 - math.abs(playerDMG)),
                    sight)
    else
        g.setColor(255, 255, 255)
    end

    -- determine if moving and move
    if who == 1 then
        if lastAttack == 'research' then
            g.draw(gfx.andrew2, 60, 140, rotation, 1, 1, 40, 40)
        else
            g.draw(gfx.andrew2, 40 - math.abs(((timer * ((2 - timerMax)/timerMax + 1)) * 20) - 20), 100)
        end
    else
        g.draw(gfx.andrew2, 20, 100)
    end

    g.setColor(255, 255, 255)
    g.rectangle('fill', 0, 180, 320, 60)
    g.setColor(0, 0, 0)
    g.rectangle('line', 0, 180, 320, 60)

    -- Draw attack menu at bottom of screen
    for key, attack in ipairs(attacks) do
        local x, y = 10, key
        if y > 2 then
            x = 185
            y = y - 2
        end
        y = 170 + (y * 20)

        g.print(attack.name, x, y)
    end

    -- Prevent goofy health bar stuff
    if hp.enemy.current <= 0 then
        hp.enemy.current = 0
    end

    if hp.player.current <= 0 then
        hp.player.current = 0
    end

    -- Draw health boxes
    g.rectangle('line', 10, 12, 60, 10)
    g.rectangle('line', 250, 162, 60, 10)

    -- Draw names
    g.setFont(fnt.combat)
    g.print(enemy[enemyCurrent].type.name, 10, 2)
    g.print('Andrew', 250, 152)

    -- Draw player health / max health
    if timer < timerMax / 2 then
        g.print(math.floor(hp.player.current + (hp.player.previous - hp.player.current) * (timer * ((2 - timerMax)/timerMax + 1))) .. "/" .. hp.player.max, 250, 170)
    else
        g.print(math.floor(hp.player.previous) .. "/" .. hp.player.max, 250, 170)
    end

    if timer < timerMax / 2 then

        -- Set color of health bar for enemy
        g.setColor(
            255 - (255 * (hp.enemy.current + ((hp.enemy.previous - hp.enemy.current) * (timer * ((2 - timerMax)/timerMax + 1)))) / hp.enemy.max),
            255 * ((hp.enemy.previous - ((hp.enemy.previous - hp.enemy.current) * (1 - (timer * ((2 - timerMax)/timerMax + 1))))) / hp.enemy.max),
            0)
        -- Draw health bar sliding for enemy
        g.rectangle('fill', 10, 14, (60 * ((hp.enemy.current + (hp.enemy.previous - hp.enemy.current) * (timer * ((2 - timerMax)/timerMax + 1)))/ hp.enemy.max)), 6)

        -- Set color of health bat for player
        g.setColor(
            255 - (255 * (hp.player.current + ((hp.player.previous - hp.player.current) * (timer * ((2 - timerMax)/timerMax + 1)))) / hp.player.max),
            255 * ((hp.player.previous - ((hp.player.previous - hp.player.current) * (1 - (timer * ((2 - timerMax)/timerMax + 1))))) / hp.player.max), 0)
        -- Draw health bar sliding for player
        g.rectangle('fill', 250, 164, (60 * ((hp.player.current + (hp.player.previous - hp.player.current) * (timer * ((2 - timerMax)/timerMax + 1))) / hp.player.max)), 6)

    else
        g.setColor(255 - (255 * (hp.enemy.previous / hp.enemy.max)), 255 * (hp.enemy.previous / hp.enemy.max), 0)
        g.rectangle('fill', 10, 14, (60 * ((hp.enemy.previous / hp.enemy.max))), 6)

        g.setColor(255 - (255 * (hp.player.previous / hp.player.max)), 255 * (hp.player.previous / hp.player.max), 0)
        g.rectangle('fill', 250, 164, (60 * ((hp.player.previous / hp.player.max))), 6)
    end

end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
