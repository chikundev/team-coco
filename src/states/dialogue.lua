-- chikun :: 2014
-- Dialogue state


-- Temporary state, removed at end of script
local newState = { }

local currentDialogue = ""
-- On state create
function newState:create()

    if enemyCurrent then
        for key, level in ipairs(levels) do
            if level.name == levels[lvl.current].name then
                local currentEnemy = enemy[enemyCurrent]
                currentDialogue = level.dialogue.taunts[currentEnemy.type.code][math.random(#level.dialogue.taunts[currentEnemy.type.code])]
            end
        end
        currentDialogue = tostring(currentDialogue)
    end

    if lastAttack then

        currentDialogue = dialogue.midCombat.taunts[lastAttack][math.random(#dialogue.midCombat.taunts[lastAttack])]

    end

end


-- On state update
function newState:update(dt)

    if input.check("action") and not actionPressed then

        if lastAttack then
            who = math.abs(who - 1)
            state.load(states.combatAnimation)
            actionPressed = true
        else
            state.change(states.combat)
            actionPressed = true
            who = 0
            lastAttack = nil
        end
    elseif not input.check("action") then
        actionPressed = false
    end

end


-- On state draw
function newState:draw()

    if lastAttack then
        states.combat:draw()
    else
        states.play:draw()
    end

    scale.perform()
    g.setColor(255, 255, 255)
    g.rectangle('fill', 0, 180, 320, 60)

    g.setFont(fnt.option)
    g.setColor(0, 0, 0)
    g.printf(currentDialogue, 10, 190, 280, 'left')

end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
