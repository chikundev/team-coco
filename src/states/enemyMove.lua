-- chikun :: 2014
-- Enemy movement state


-- Temporary state, removed at end of script
local newState = { }
enemyCurrent = nil

-- On state create
function newState:create()

    levels[lvl.current].bgm:stop()

end


-- On state update
function newState:update(dt)

    for key, object in ipairs(enemy) do
        if math.overlap(object.wall, player) then
            local xMove = 0
            local yMove = 0
            if object.direction == 0 then
                yMove = -1
            elseif object.direction == 90 then
                xMove = 1
            elseif object.direction == 180 then
                yMove = 1
            else
                xMove = -1
            end
            object.x = object.x + 256 * xMove * dt
            object.y = object.y + 256 * yMove * dt
            if math.overlap(object, player) then
                enemyCurrent = key
                state.load(states.dialogue)
            end
        end
    end

end


-- On state draw
function newState:draw()

    states.play:draw()


end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
