-- chikun :: 2014
-- Combat state for enemy


-- Temporary state, removed at end of script
local newState = { }
dmgMultiEnemy = 1

-- On state create
function newState:create()

end


-- On state update
function newState:update(dt)


    local chance = math.random(100)

    -- Prevent combat in case of someone already being dead
    if hp.enemy.current <= 0 and hp.player.current > 0 then
        table.remove(enemy, enemyCurrent)
        enemyCurrent = nil
        if flags.enableBGM then
            levels[lvl.current].bgm:play()
        end
        state.set(states.play)
    elseif hp.player.current <= 0 and hp.enemy.current > 0 then
        states.play:kill()
        state.change(states.menu)

    -- Attack for hater
    elseif enemy[enemyCurrent].type.name == "Hater" then
        hp.player.previous = hp.player.current
        hp.enemy.previous = hp.enemy.current

        if chance <= 30 then
            lastAttack = 'argument'
            hp.player.current = hp.player.current - (math.random(2) + 8) * dmgMultiEnemy
            state.load(states.dialogue)

        elseif chance <= 60 then
            lastAttack = 'fallacy'
            hp.player.current = hp.player.current - (math.random(2) + 8) * dmgMultiEnemy * 1.2
            state.load(states.dialogue)

        elseif chance <= 90 then
            lastAttack = 'research'
            dmgMultiEnemy = dmgMultiEnemy * 1.1
            state.load(states.dialogue)

        else

            -- counter fallacy successfull
            if lastAttack == 'fallacy' then
                lastAttack = 'counterFallacy'
                hp.player.current = hp.player.current - (math.random(2) + 8) * dmgMultiEnemy * 2
                hp.enemy.current = hp.enemy.current + (math.random(2) + 8) * dmgMultiEnemy
                state.load(states.dialogue)

            -- counter fallacy failure
            else
                lastAttack = 'counterFallacy'
                hp.enemy.current = hp.enemy.current - (math.random(2) + 8) * dmgMultiEnemy * 2
                state.load(states.dialogue)
            end
            lastAttack = 'counterFallacy'
        end

    -- Attack chance for troll
    elseif enemy[enemyCurrent].type.name == "Troll" then

        if chance <= 20 then
            lastAttack = 'argument'
            hp.player.current = hp.player.current - (math.random(2) + 8) * dmgMultiEnemy
            state.load(states.dialogue)

        elseif chance <= 70 then
            lastAttack = 'fallacy'
            hp.player.current = hp.player.current - (math.random(2) + 8) * dmgMultiEnemy * 1.2
            state.load(states.dialogue)

        elseif chance <= 90 then
            lastAttack = 'research'
            dmgMultiEnemy = dmgMultiEnemy * 1.1
            state.load(states.dialogue)

        else
            lastAttack = 'counterFallacy'

            -- counter fallacy successful
            if lastAttack == 'fallacy' then
                hp.player.current = hp.player.current - (math.random(2) + 8) * dmgMultiEnemy * 2
                hp.enemy.current = hp.enemy.current + (math.random(2) + 8) * dmgMultiEnemy
                state.load(states.dialogue)

            -- counter fallacy failure
            else
                hp.enemy.current = hp.enemy.current - (math.random(2) + 8) * dmgMultiEnemy * 2
                state.load(states.dialogue)
            end
        end
    end

end


-- On state draw
function newState:draw()

    states.combat:draw()

    scale.perform()
    g.setColor(255, 255, 255)
    g.rectangle('fill', 0, 180, 360, 60)
    g.setColor(0, 0, 0)
    g.rectangle('line', 0, 180, 320, 60)

end


-- On state kill
function newState:kill()

    states.combat:kill()

end


-- Transfer data to state loading script
return newState
