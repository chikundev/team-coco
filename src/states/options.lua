-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local menuState = { }

-- On state create
function menuState:create()

    menuVars = {
        selected = 1,
        timer    = 0

    }

end

-- local variables
local musicToggle = 'on'
local SFXToggle = 'on'

-- On state update
function menuState:update(dt)

    -- Preven speed scrolling in menu
    if menuVars.timer == 0 then

        -- Check if we are trying to scroll through menu
        if input.check("down") or input.check("up") then
            local dir = 0

            -- Check if scrolling down
            if input.check("down") then
                dir = dir + 1
            end

            -- Check if scrolling up
            if input.check("up") then
                dir = dir - 1
            end

            -- Scroll in appropriate direction
            menuVars.selected = menuVars.selected + dir

            -- Send scroll to start if run out
            if menuVars.selected >= 5 then
                menuVars.selected = 1
            end

            -- Send scroll to end if run out
            if menuVars.selected <= 0 then
                menuVars.selected = 4
            end

            -- Set timer to prevent speed Scrolling
            if dir ~= 0 then
                menuVars.timer = 0.2
            end
        end
    else

        -- Reset timer to allow button mash scrolling
        if not (input.check("down") or input.check("up")) then
            menuVars.timer = 0
        end

        -- Coun down timer
        menuVars.timer = menuVars.timer - dt

        -- Prevent timer becoming less than zero
        if menuVars.timer <= 0 then
            menuVars.timer = 0
        end
    end

    -- Do things upon action for different menus
    if input.check("action") and not flags.actionPressed then

        -- Toggle BGM
        if menuVars.selected == 1 then

            flags.enableBGM = not flags.enableBGM

            if musicToggle == 'on' then
                musicToggle = 'off'
            else
                musicToggle = 'on'
            end

        -- Toggle SFX
        elseif menuVars.selected == 2 then

            flags.enableSFX = not flags.enableSFX

            if SFXToggle == 'on' then
                SFXToggle = 'off'
            else
                SFXToggle = 'on'
            end

        -- Toggle Fullscreen
        elseif menuVars.selected == 3 then

            w.setFullscreen(not w.getFullscreen(), "desktop")

        -- Back to main menu
        elseif menuVars.selected == 4 then

            state.change(states.menu)
            menuVars.selected = 2

        end

    end

end


-- On state draw
function menuState:draw()

    -- Perform scaling
    scale.perform()

    -- Set font
    g.setFont(fnt.option)

    -- Draw bg
    g.setColor(255, 55, 55)

    g.rectangle("fill", 0, 0, 320, 240)

    -- Draw bg
    g.setColor(55, 255, 55)

    g.rectangle("fill", 160, 120, 320, 240)

    -- Draw bg
    g.setColor(255, 255, 255)

    -- Update scaling
    scale.perform()

    -- Options menu options
    local opts = {
        "Music: " .. tostring(musicToggle),
        "SFX: " .. tostring(SFXToggle),
        "Fullscreen: " .. tostring(w.getFullscreen()),
        "Back"
    }

    for key, value in ipairs(opts) do
        g.print(value, 26, key * 26)
    end

    local x, y, w, h =
        24,
        26 * menuVars.selected - 2,
        g.getFont():getWidth(opts[menuVars.selected]) + 4,
        g.getFont():getHeight() + 4

    g.rectangle("line", x, y, w, h)

end


-- On state kill
function menuState:kill()

    menuVars = nil

end


-- Transfer data to state loading script
return menuState
