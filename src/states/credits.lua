-- chikun :: 2014
-- Credits code


-- Temporary state, removed at end of script
local newState = { }
local timer = 45
local credits = [[
Lead Programmer:
Bradley Roope
Assistant Programmer:
Josef Frank

Sound Designer:
Cohen Dennis

Graphic Designer:
Bradley Roope

Special Thanks:
Ryan Little


Made by chikun™ 2014
]]



-- On state create
function newState:create()

end


-- On state update
function newState:update(dt)

    if timer <= 0 then
        state.change(states.lvlSelect)
    else
        timer = timer - dt
    end

end


-- On state draw
function newState:draw()

    scale.perform()
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 0, 320, 240)
    g.setColor(128, 0, 0)
    g.print(credits, 20, 8 * (timer + 15) - 240)

end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
