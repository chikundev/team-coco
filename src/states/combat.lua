-- chikun :: 2014
-- Combat state


-- Temporary state, removed at end of script
local newState = { }
local selected = 1
who = 0
lastAttack = nil
hp = {
    player = {
        max      = 0,
        previous = 0,
        current  = 0
    },
    enemy = {
        max      = 0,
        previous = 0,
        current  = 0
    }
}

local dmgMulti = 1

-- On state create
function newState:create()

    -- Variable needed for this state
    arrowPressed = false
    dmgMulti = 1
    dmgMultiEnemy = 0.9 + (lvl.unlocked * 0.1)

    -- Set health of player
    hp.player.max      = player.hp
    hp.player.current  = player.hp
    hp.player.previous = player.hp

    -- Set health of enemy
    if enemy[enemyCurrent].type.name == "Hater" then
        hp.enemy.max      = 140 + (lvl.unlocked * 10)
        hp.enemy.current  = 140 + (lvl.unlocked * 10)
        hp.enemy.previous = 140 + (lvl.unlocked * 10)
    elseif enemy[enemyCurrent].type.name == "Troll" then
        hp.enemy.max      = 90 + (lvl.unlocked * 10)
        hp.enemy.current  = 90 + (lvl.unlocked * 10)
        hp.enemy.previous = 90 + (lvl.unlocked * 10)
    end

    attacks = {
        {
            name = "Argument"
        },
        {
            name = "Fallacious Argument"
        },
        {
            name = "Counter Fallacy"
        },
        {
            name = "Research"
        }
    }

end


-- On state update
function newState:update(dt)

    -- Prevent combat if opponent is defeated
    if hp.enemy.current <= 0 and hp.player.current > 0 then
        table.remove(enemy, enemyCurrent)
        enemyCurrent = nil
        if flags.enableBGM then
            levels[lvl.current].bgm:play()
        end
        state.set(states.play)

    -- Prevent combat if your defeated
    elseif hp.player.current <= 0 and hp.enemy.current > 0 then
        states.play:kill()
        state.change(states.menu)
    end

    -- Combat selection
    if not arrowPressed then
        if input.check("up") or input.check("down") then
            if selected % 2 == 0 then
                selected = selected - 1
            else
                selected = selected + 1
            end
            arrowPressed = true
        elseif input.check("right") or input.check("left") then
            if selected < 3 then
                selected = selected + 2
            else
                selected = selected - 2
            end
            arrowPressed = true
        end
    elseif not (input.check("up") or input.check("down") or input.check("right") or input.check("left")) then
        arrowPressed = false
    end

    if input.check("action") and not actionPressed then

        hp.enemy.previous = hp.enemy.current
        hp.player.previous = hp.player.current

        -- Using argument
        if selected == 1 then
            hp.enemy.current = hp.enemy.current - (math.random(2) + 8) * dmgMulti
            lastAttack = 'argument'

        -- Using fallacious argument
        elseif selected == 2 then
            hp.enemy.current = hp.enemy.current - (math.random(2) + 8) * dmgMulti * 1.2
            lastAttack = 'fallacy'

        -- Using counter fallacy
        elseif selected == 3 then
            -- Successful
            if lastAttack == 'fallacy' then
                hp.enemy.current = hp.enemy.current - (math.random(2) + 8) * dmgMulti * 2
                hp.player.current = hp.player.current + (math.random(2) + 8) * dmgMulti

            -- Failure
            else
                hp.player.current = hp.player.current - (math.random(2) + 8) * dmgMulti * 2
            end
            lastAttack = 'counterFallacy'
        elseif selected == 4 then
            dmgMulti = dmgMulti * 1.1
            lastAttack = 'research'
        end
        state.load(states.dialogue)
        actionPressed = true
    elseif actionPressed and not input.check("action") then
        actionPressed = false
    end

    if hp.enemy.current <= 0 then
        hp.enemy.current = 0
    elseif hp.player.current <= 0 then
        hp.player.current = 0
    end

end


-- On state draw
function newState:draw()

    scale.perform()
    g.setFont(fnt.option)
    g.setColor(255, 255, 255)
    g.rectangle('fill', 0, 0, 320, 240)

    g.draw(enemy[enemyCurrent].type.imageCombat, 220, 10)
    g.draw(gfx.andrew2, 20, 100)

    g.setColor(0, 0, 0)
    g.rectangle('line', 0, 180, 320, 60)

    for key, attack in ipairs(attacks) do
        local x, y = 10, key
        if y > 2 then
            x = 185
            y = y - 2
        end
        y = 170 + (y * 20)

        g.print(attack.name, x, y)
        if key == selected then
            g.rectangle('line', x - 2, y - 2, g.getFont():getWidth(attack.name) + 4, g.getFont():getHeight() + 4)
        end
    end

    if hp.enemy.current <= 0 then
        hp.enemy.current = 0
    end

    if hp.player.current <= 0 then
        hp.player.current = 0
    end

    g.rectangle('line', 10, 12, 60, 10)
    g.rectangle('line', 250, 162, 60, 10)

    g.setFont(fnt.combat)
    g.print(enemy[enemyCurrent].type.name, 10, 2)
    g.print('Andrew', 250, 152)
    g.print(math.floor(hp.player.previous) .. "/" .. hp.player.max, 250, 170)

    g.setColor(255 - (255 * (hp.enemy.previous / hp.enemy.max)), 255 * (hp.enemy.previous / hp.enemy.max), 0)
    g.rectangle('fill', 10, 14, (60 * ((hp.enemy.previous / hp.enemy.max))), 6)

    g.setColor(255 - (255 * (hp.player.previous / hp.player.max)), 255 * (hp.player.previous / hp.player.max), 0)
    g.rectangle('fill', 250, 164, (60 * ((hp.player.previous / hp.player.max))), 6)
end


-- On state kill
function newState:kill()

    lastAttack = nil
    who = 0
    dmgMulti = 1
    dmgMultiEnemy = 1

end


-- Transfer data to state loading script
return newState
