-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local menuState = { }
local opts = { }

-- On state create
function menuState:create()

    menuVars = {
        selected = 1,
        timer    = 0

    }

    for i = 1, lvl.unlocked do
        opts[i] = levels[i].name
    end
    --for key, lvl in ipairs(levels) do
    --    opts[#opts + 1] = lvl.name
    --end

    opts[#opts + 1] = "Main Menu"

end


-- On state update
function menuState:update(dt)

    -- Preven speed scrolling in menu
    if menuVars.timer == 0 then

        -- Check if we are trying to scroll through menu
        if input.check("down") or input.check("up") then
            local dir = 0

            -- Check if scrolling down
            if input.check("down") then
                dir = dir + 1
            end

            -- Check if scrolling up
            if input.check("up") then
                dir = dir - 1
            end

            -- Scroll in appropriate direction
            menuVars.selected = menuVars.selected + dir

            -- Send scroll to start if run out
            if menuVars.selected > #opts then
                menuVars.selected = 1
            end

            -- Send scroll to end if run out
            if menuVars.selected <= 0 then
                menuVars.selected = #opts
            end

            -- Set timer to prevent speed Scrolling
            if dir ~= 0 then
                menuVars.timer = 0.2
            end
        end
    else

        -- Reset timer to allow button mash scrolling
        if not (input.check("down") or input.check("up")) then
            menuVars.timer = 0
        end

        -- Coun down timer
        menuVars.timer = menuVars.timer - dt

        -- Prevent timer becoming less than zero
        if menuVars.timer <= 0 then
            menuVars.timer = 0
        end
    end

    -- Do things upon action for different menus
    if input.check("action") and not flags.actionPressed then

        if menuVars.selected <= lvl.unlocked then
            level.load(menuVars.selected)

        -- Back to main menu
        elseif opts[menuVars.selected] == "Main Menu" then

            state.change(states.menu)
            menuVars.selected = 1

        end

    end

end


-- On state draw
function menuState:draw()

    -- Perform scaling
    scale.perform()

    -- Set font
    g.setFont(fnt.option)

    -- Draw bg
    g.setColor(255, 55, 55)

    g.rectangle("fill", 0, 0, 320, 240)

    -- Draw bg
    g.setColor(55, 255, 55)

    g.rectangle("fill", 160, 120, 320, 240)

    -- Draw bg
    g.setColor(255, 255, 255)

    -- Update scaling
    scale.perform()

    for key, value in ipairs(opts) do
        g.print(value, 26, key * 26)
    end

    local x, y, w, h =
        24,
        26 * menuVars.selected - 2,
        g.getFont():getWidth(opts[menuVars.selected]) + 4,
        g.getFont():getHeight() + 4

    g.rectangle("line", x, y, w, h)

end


-- On state kill
function menuState:kill()

    opts = { }

    menuVars = nil

end


-- Transfer data to state loading script
return menuState
