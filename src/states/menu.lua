-- chikun :: 2014
-- Menu state


-- Temporary state, removed at end of script
local menuState = { }


-- On state create
function menuState:create()

    menuVars = {
        selected  = 1,
        timer     = 0,
    }

end


-- On state update
function menuState:update(dt)

    -- Preven speed scrolling in menu
    if menuVars.timer == 0 then

        -- Check if we are trying to scroll through menu
        if input.check("down") or input.check("up") then
            local dir = 0

            -- Check if scrolling down
            if input.check("down") then
                dir = dir + 1
            end

            -- Check if scrolling up
            if input.check("up") then
                dir = dir - 1
            end

            -- Scroll in appropriate direction
            menuVars.selected = menuVars.selected + dir

            -- Send scroll to start if run out
            if menuVars.selected >= 4 then
                menuVars.selected = 1
            end

            -- Send scroll to end if run out
            if menuVars.selected <= 0 then
                menuVars.selected = 3
            end

            -- Set timer to prevent speed Scrolling
            if dir ~= 0 then
                menuVars.timer = 0.2
            end
        end
    else

        -- Reset timer to allow button mash scrolling
        if not (input.check("down") or input.check("up")) then
            menuVars.timer = 0
        end

        -- Coun down timer
        menuVars.timer = menuVars.timer - dt

        -- Prevent timer becoming less than zero
        if menuVars.timer <= 0 then
            menuVars.timer = 0
        end
    end

    -- Do things upon action for different menus
    if input.check("action") and not flags.actionPressed then

        -- Will become option to go to play state
        if menuVars.selected == 1 then

            state.change(states.room)

        -- Will become option to go to options menu
        elseif menuVars.selected == 2 then

            state.change(states.options)

        -- Leaves game if want to quit
        elseif menuVars.selected == 3 then

            e.quit()

        end

    end

end


-- On state draw
function menuState:draw()

    -- Perform scaling
    scale.perform()

    -- Set font
    g.setFont(fnt.option)

    -- Draw bg
    g.setColor(255, 55, 55)

    g.rectangle("fill", 0, 0, 320, 240)

    -- Draw bg
    g.setColor(55, 255, 55)

    g.rectangle("fill", 160, 120, 320, 240)

    -- Draw bg
    g.setColor(255, 255, 255)

    -- Drawing three menu options
    g.print("Play", 26, 26)
    g.print("Options", 26, 52)
    g.print("Exit", 26, 78)

    -- Draw selection box for menu
    if menuVars.selected ~= 2 then
        g.rectangle("line", 24, (menuVars.selected * 26) - 2, 34, 26)
    else
        g.rectangle("line", 24, (menuVars.selected * 26) - 2, 60, 26)
    end

end


-- On state kill
function menuState:kill()

    -- Kill menuVars
    menuVars = nil

end


-- Transfer data to state loading script
return menuState
