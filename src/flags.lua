-- chikun :: 2014
-- Flags to control certain variables


-- Table of game flags
flags = {
    enableLighting   = false,
    enableScaling    = true,
    enableBGM        = true,
    enableSFX        = true,
    actionPressed    = false
}
