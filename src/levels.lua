-- chikun :: 2014
-- Level specific things


level = { }

levels = {
    {
        name = "Spacebook",
        map = maps.spacebook,
        bgm = bgm.spacebook,
        dialogue = dialogue.facebook
    },
    {
        name = "Leaddit",
        map = maps.leaddit,
        bgm = bgm.reddit,
        dialogue = dialogue.reddit
    },
    {
        name = "Flitter",
        map = maps.flitter,
        bgm = bgm.spacebook,
        dialogue = dialogue.facebook
    },
    {
        name = "Dumblr",
        map = maps.dumblr,
        bgm = bgm.reddit,
        dialogue = dialogue.reddit
    },
    {
        name = "DupeTube",
        map = maps.dupetube,
        bgm = bgm.spacebook,
        dialogue = dialogue.facebook
    }
}

lvl = {
    current = 0,
    unlocked = 1
}

function level.load(num)

    map.current = levels[num].map
    lvl.current = num
    if flags.enableBGM then
        levels[num].bgm:play()
    end
    state.change(states.play)

end
