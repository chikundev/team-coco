-- chikun :: 2014
-- Player code


-- Table for player
player = { }

-- Creation of player
function player.create(pX, pY, pW, pH)

    player.x  = pX
    player.y  = pY
    player.w  = pW
    player.h  = pH
    player.hp = 200

end

-- Update for the player
function player.update(dt)

    local xMove, yMove = 0, 0

    if input.check("left") then
        xMove = - 1
    end
    if input.check("right") then
        xMove = xMove + 1
    end

    if input.check("up") then
        yMove = - 1
    end
    if input.check("down") then
        yMove = yMove + 1
    end

    player.x = player.x + (xMove * 256 * dt)

    while math.overlapTable(collisions, player) or math.overlapTable(enemy, player) do
        player.x = math.round(player.x - xMove)
    end

    player.y = player.y + (yMove * 256 * dt)

    while math.overlapTable(collisions, player) or math.overlapTable(enemy, player) do
        player.y = math.round(player.y - yMove)
    end

    for key, object in ipairs(enemy) do
        if math.overlap(object.wall, player) then
            state.load(states.enemyMove)
        end
    end

    if math.overlap(player, exit.wall) then
        if levels[lvl.unlocked].name == levels[lvl.current].name then
            lvl.unlocked = lvl.unlocked + 1
            if lvl.unlocked == 6 then
                lvl.unlocked = 5
                state.change(states.credits)
            else
                state.change(states.lvlSelect)
            end
        else
            state.change(states.lvlSelect)
        end
    end

end

-- Draw of player
function player.draw()

    g.setColor(0,0,0)
    g.rectangle("fill", player.x, player.y, player.w, player.h)

end
