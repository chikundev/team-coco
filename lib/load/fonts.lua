-- chikun :: 2014
-- Loads all fonts from the /fnt folder


-- Font multiplier
local multi = 1

love.graphics.setDefaultFilter("nearest")

-- Can't do this recursively due to sizes
fnt = {
    -- Splash screen font
    splash  = love.graphics.newFont("fnt/exo2.otf", 32 * multi),
    option  = love.graphics.newFont("fnt/exo2.otf", 16 * multi),
    combat  = love.graphics.newFont("fnt/exo2.otf", 10  * multi)
}
