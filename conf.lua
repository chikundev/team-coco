-- chikun :: 2014
-- Configuration file


function love.conf(game)

    -- Attach a console for Windows debug [DISABLE ON DISTRIBUTION]
    game.console    = true

    -- Version of LÖVE which this game was made for
    game.version    = "0.9.1"

    -- Omit modules due to disuse
    game.modules.math   = false
    game.modules.thread = false

    -- Various window settings

    game.window.title   = "chikun Game"
    game.window.width   = 640
    game.window.height  = 480
    game.window.minwidth   = 320
    game.window.minheight  = 240
    game.window.resizable = true

end
